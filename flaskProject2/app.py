from flask import Flask, render_template, request, redirect, url_for, flash
from flask_sqlalchemy import SQLAlchemy
from form import LoginForm,EmployeeForm
from flask_wtf import FlaskForm
from flask_mysqldb import MySQL
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from werkzeug.exceptions import BadRequestKeyError



app = Flask(__name__)

client=app.test_client()
app.secret_key = "123456"




#SqlAlchemy Database Configuration With Mysql
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:''@localhost/my'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


db = SQLAlchemy(app)
migrate=Migrate(app, db)

manager=Manager(app)
manager.add_command('db', MigrateCommand)


#Creating model table for our CRUD database
class Data(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(100))
    position = db.Column(db.String(100))
    date_of_birth = db.Column(db.String(100))
    salary = db.Column(db.Integer)



    def __init__(self, username, position, date_of_birth,salary):

        self.username = username
        self.position = position
        self.date_of_birth = date_of_birth
        self.salary = salary

class Department(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    department = db.Column(db.String(100))




    def __init__(self, department):

        self.username = department







#This is the index route where we are going to
#query on all our employee data
@app.route('/index')
def Index():
    all_data = Data.query.all()

    return render_template("index.html", employees = all_data)



#this route is for inserting data to mysql database via html forms
@app.route('/insert', methods = ['POST'])
def insert():
    if request.method=='POST':
        try:
            username = request.form['username']
            position = request.form['position']
            date_of_birth = request.form['date_of_birth']
            salary = request.form['salary']



            my_data = Data(username, position, date_of_birth,salary)
            db.session.add(my_data)
            db.session.commit()

            flash("Employee Inserted Successfully")
            
            

            return redirect(url_for('Index'))
        except BadRequestKeyError as ex:
            return 'Unknown key: "{}"'.format(ex.args[0]), 500


    #this is our update route where we are going to update our employee
@app.route('/update', methods = ['GET', 'POST'])
def update():

    if request.method == 'POST':
        my_data = Data.query.get(request.form.get('id'))

        my_data.username = request.form['username']
        my_data.position = request.form['position']
        my_data.date_of_birth = request.form['date_of_birth']
        my_data.salary = request.form['salary']


        db.session.commit()
        flash("Employee Updated Successfully")

        return redirect(url_for('Index'))




#This route is for deleting our employee
@app.route('/delete/<id>/', methods = ['GET', 'POST'])
def delete(id):
    my_data = Data.query.get(id)
    db.session.delete(my_data)
    db.session.commit()
    flash("Employee Deleted Successfully")

    return redirect(url_for('Index'))

@app.route('/')
def Emp():
    #name='NASTYA'
    context={
        'text':'This is first application Flask',
        'name':'Nastya'

    }
    return render_template('main.html', data=context)



@app.route('/login', methods=['GET','POST'])
def Login():
    form = LoginForm()
    if request.method == 'POST':
        department = request.form['department']
        my_data = Department(department)
        db.session.add(my_data)
        db.session.commit()

        flash("Department Inserted Successfully")

    return render_template('login.html', title='Department', form=form)



@app.route('/employee', methods=['GET','POST'])
def Employee():
    form = EmployeeForm()
    if request.method=='POST':
        username = request.form['username']
        position = request.form['position']
        date_of_birth = request.form['date_of_birth']
        salary = request.form['salary']
        my_data = Data(username, position, date_of_birth, salary)
        db.session.add(my_data)
        db.session.commit()

        flash("Employee Inserted Successfully")


    return render_template('employee.html', title='Employee', form=form)



@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html')


@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html')



@app.route('/about')
def about():
    return render_template('about.html')



if __name__ == "__main__":
    app.run(debug=True)
    #manager.run()