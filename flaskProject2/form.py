from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, DateField, IntegerField
from wtforms.validators import InputRequired


class LoginForm(FlaskForm):
    department = StringField('Department', validators=[InputRequired()])


class EmployeeForm(FlaskForm):
    username = StringField('Username', validators=[InputRequired()])
    position = StringField('Position', validators=[InputRequired()])
    date_of_birth = DateField('Date Of Birth', validators=[InputRequired()])
    salary = IntegerField('Salary', validators=[InputRequired()])

