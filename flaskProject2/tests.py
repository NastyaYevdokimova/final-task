from app import client
from app import Data


def test_get():
    res=client.get('/')
    assert res.status_code==200

def test_get1():
    res=client.get('/index')
    assert res.status_code==200

def test_get2():
    res=client.get('/insert')
    assert res.status_code==405


def test_get3():
    res=client.get('/update')
    assert res.status_code==200

def test_get4():
    res=client.get('/delete/<id>/')
    assert res.status_code==200

def test_get5():
    res=client.get('/employee')
    assert res.status_code==200


def test_get6():
    res=client.get('404')
    assert res.status_code==200

def test_get7():
    res=client.get('500')
    assert res.status_code==200

def test_get8():
    res=client.get('/about')
    assert res.status_code==200

def test_user_schema1(session):
    username = 'username'
    position = 'position'
    date_of_birth = '1777-01-01'
    salary = 1235
    uu = Data(username=username,position=position,date_of_birth=date_of_birth,salary=salary)
    session.add(uu)
    session.commit()

    assert uu.id==1
    assert uu.username==username
    assert uu.position == position
    assert uu.date_of_birth == date_of_birth
    assert uu.salary == salary